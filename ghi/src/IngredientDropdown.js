import { selected } from "./store/filterSlice";
import { useDispatch } from "react-redux";
import { useGetAllIngredientsQuery } from "./store/apiSlice";
import CocktailsList from "./CocktailsList";



const IngredientsDropdown = () => {
  const dispatch = useDispatch();
  const { data, isLoading } = useGetAllIngredientsQuery();
  const handleIngredientChange = (event) => {
    const selectedValue = event.target.value;
    const reset = "All Ingredients"
    if (selectedValue ===  reset) {
      return (<CocktailsList/>)
    } else {
      dispatch(selected(selectedValue));
    }
  };

  if (isLoading) return <div>Loading...</div>;

  return (
    <>
      <select className="form-select form-select-sm" aria-label=".form-select-sm example" onChange={handleIngredientChange}>
        <option>All Ingredients</option>
        {data.map((item) => (
          <option key={item.ingredient} value={item.ingredient}>
            {item.ingredient}
          </option>
        ))}
      </select>
    </>
  );
};

export default IngredientsDropdown;
