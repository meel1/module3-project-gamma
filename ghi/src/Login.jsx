import { useLoginMutation } from "./store/apiSlice";
import { useState, useEffect } from "react";
import { useNavigate } from "react-router-dom";
import AlertError from "./AlertError";
import "./login.css";
import { useDispatch } from "react-redux";
import { reset } from "./store/searchSlice";
import { resetIngredientSearch } from "./store/filterSlice";
import { Link } from "react-router-dom";


const Login = () => {
  const navigate = useNavigate();
  const [login, loginResult] = useLoginMutation();
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [errorMessage, setErrorMessage] = useState("");
  const dispatch = useDispatch();

  useEffect(() => {
    if (loginResult.error) {
      if (loginResult.error.status === 401) {
        setErrorMessage(loginResult.error.data.detail);
      }
      if (loginResult.error.status === 422) {
        setErrorMessage(loginResult.error.data.detail);
      }
    }
    if (loginResult.isSuccess) navigate("/user");
  }, [loginResult, navigate]);

  const handleSubmit = (e) => {
    e.preventDefault();
    login({ username, password });
    dispatch(reset());
    dispatch(resetIngredientSearch());
  };

  return (
    <div className="container-login">
      <div className="d-flex justify-content-center h-100">
        <div className="card-login">
          <div className="card-header-login login">
            <h1 className="card-header-login">Login</h1>
            {errorMessage && <AlertError>{errorMessage}</AlertError>}
            <div className="d-flex justify-content-center h-100">
              <form onSubmit={handleSubmit}>
                <div className="mb-3 input-group form-group">
                  <div className="input-group-prepend">
                    <span className="input-group-text">
                      <i className="fas fa-user"></i>
                    </span>
                  </div>
                  <input
                    type="text"
                    className="form-control"
                    id="Login__username"
                    value={username}
                    onChange={(e) => setUsername(e.target.value)}
                    placeholder="username ex: mixmaster007"
                    required
                  />
                </div>
                <div className="mb-3 input-group form-group">
                  <div className="input-group-prepend">
                    <span className="input-group-text">
                      <i className="fas fa-key"></i>
                    </span>
                  </div>
                  <input
                    type="password"
                    className="form-control"
                    id="Login__password"
                    value={password}
                    onChange={(e) => setPassword(e.target.value)}
                    placeholder="password ex: ********"
                    required
                  />
                </div>
                <div className="form-group">
                  <div className="form-check">
                    <input
                      type="checkbox"
                      className="form-check-input"
                      id="dropdownCheck"
                    />
                    <label
                      className="form-check-label remember"
                      htmlFor="dropdownCheck"
                    >
                      Remember me
                    </label>
                  </div>
                </div>
                <button
                  type="submit"
                  className="btn btn-warning btn btn-block mb-4"
                >
                  Log In
                </button>
                <div className="text-center">
                  <p>Don't have an account? <Link to="/signup">Sign Up</Link></p>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Login;
