import { Link } from "react-router-dom";

const CocktailCard = ({ name, url }) => {
  return (
    <Link to={`/api/cocktails/name/${name}`} className="col-lg-4 col-md-6 mb-4 card-link">
      <div className="card-base mb-3 shadow h-100">
        <img
          src={url}
          className="card-img-top"
          alt=""
          style={{ maxWidth: "100%", maxHeight: "100%", objectFit: "cover" }}
        />
        <div className="card-body-base">
          <h5 className="card-title-base mb-auto">{name}</h5>
        </div>
      </div>
    </Link>
  );
};

export default CocktailCard;
