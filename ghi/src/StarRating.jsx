import React, { useState, useEffect } from "react";
import { FaStar } from "react-icons/fa";
import styled from "styled-components";
import "./StarRating.css";

const Container = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  min-height: 5vh;
  font-size: 40px;
`;

const Radio = styled.input`
  display: none;
`;

const Rating = styled.div`
  cursor: pointer;
`;

const StarRating = ({ cocktailId }) => {
  const [starRating, setStarRating] = useState(() => {
    const cocktailRatings =
      JSON.parse(localStorage.getItem("cocktailRatings")) || {};
    return cocktailRatings[cocktailId] || 0;
  });

  const handleRatingChange = (givenRating) => {
    setStarRating(givenRating === starRating ? 0 : givenRating);
    alert(`Are you sure you want to give ${givenRating} stars ?`);
  };

  useEffect(() => {
    const cocktailRatings =
      JSON.parse(localStorage.getItem("cocktailRatings")) || {};
    cocktailRatings[cocktailId] = starRating;
    localStorage.setItem("cocktailRatings", JSON.stringify(cocktailRatings));
  }, [starRating, cocktailId]);

  return (
    <Container>
      {[...Array(5)].map((item, index) => {
        const givenRating = index + 1;
        return (
          <label key={index}>
            <Radio
              type="radio"
              value={givenRating}
              onClick={() => handleRatingChange(givenRating)}
            />
            <Rating>
              <FaStar
                color={
                  givenRating <= starRating ? "yellow" : "rgb(192, 192, 192)"
                }
              />
            </Rating>
          </label>
        );
      })}
    </Container>
  );
};

export default StarRating;
