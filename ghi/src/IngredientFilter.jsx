import CocktailsListByIngredient from "./IngredientSearch";



const IngredientFilter = () => {
  return (
    <>
      <CocktailsListByIngredient />
    </>
  );
};

export default IngredientFilter;
