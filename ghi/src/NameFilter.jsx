import CocktailsList from './CocktailsList';
import NameSearch from "./NameSearch";

const NameFilter = () => {
    return (
        <main className="nav-padding">
            <NameSearch />
            <CocktailsList/>
        </main>
    )
};

export default NameFilter;
