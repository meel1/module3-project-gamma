import React from "react";
import { Link } from "react-router-dom";
import FavoriteButtons from "./FavoriteButtons";
import StarRating from "./StarRating";
import "./CocktailDetailCard.css";
import "./CocktailDetail.css";


const CocktailDetailCard = ({ data, account, id, cocktailId }) => {
  const parseIngredients = (ingredientsMeasurement) => {
    const ingredientsList = ingredientsMeasurement.split(", ");
    return ingredientsList.map((ingredient) => (
      <li
        key={ingredient}
        className="list-group-item"
        style={{ marginBottom: "0px" }}
      >
        -{ingredient}
      </li>
    ));
  };

  return (
    <section className="dark">
      <div className="card-container-detail" style={{ maxWidth: "1000px" }}>
        <div className="card-detail dark-blue-bg">
          <div className="row">
            <div className="col-md-4">
              <img
                src={data.url}
                className="card-img-top img-fluid card-media"
                alt=""
              />
            </div>
            <div className="col-md-8">
              <div className="row">
                <div className="col-8">
                  <h1 className="card-title">{data.name.toUpperCase()}</h1>
                </div>
                <div className="col-4 text-end">
                  {account ? (
                    <FavoriteButtons id={data.id} />
                  ) : (
                    <Link
                      to={`/login?redirect=/api/cocktail/${id}`}
                      className="btn btn-primary"
                    >
                      Login
                    </Link>
                  )}
                </div>
              </div>
              <div className="row mb-3">
                <div className="col-12">
                  <h5 className="card-subtitle">Instructions</h5>
                  <ul>
                    <li className="list-group-item">{data.instructions}</li>
                  </ul>
                </div>
              </div>
              <div className="row mb-3">
                <div className="col-12">
                  <h5 className="card-subtitle">Ingredient Measurements</h5>
                  <ul>{parseIngredients(data.ingredients_measurement)}</ul>
                </div>
              </div>
              <div className="row">
                <div className="col-12">
                  <h5 className="card-subtitle">Category</h5>
                  <ul>
                    <li className="list-group-item">{data.category}</li>
                  </ul>
                </div>
              </div>
              <div className="row">
                <div className="col-10">
                  <div className="star-container">
                    <StarRating cocktailId={data.id} />
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
};

export default CocktailDetailCard;
