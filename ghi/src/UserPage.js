import { useGetOneRandomCocktailQuery, useGetAccountQuery } from "./store/apiSlice";
import CocktailCard from "./CocktailCard";

const UserHome = () => {
    const { data: cocktail, error, isLoading } = useGetOneRandomCocktailQuery();
    const { data: account } = useGetAccountQuery();


    if (isLoading) return <div>Loading...</div>;
    if (error) return <div>Error: {error.message}</div>;

    return (
        <div className="px-4 py-5 my-5 text-center">
            <h1 className="display-5 fw-bold text-light">Welcome back {account?.username}!</h1>
            <div className="col-lg-6 mx-auto">
                <p className="lead mb-4">
                    What drinks should we whip up today?
                </p>
            </div>
            <div className="row justify-content-center">
                <CocktailCard
                    key={cocktail.id}
                    name={cocktail.name}
                    url={cocktail.url}
                />
            </div>
        </div>

    );
};

export default UserHome;
