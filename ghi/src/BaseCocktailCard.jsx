import React from "react";
import "./BaseCocktailCard.css";

const BaseCocktailCard = ({ name, url }) => {
  return (
    <div className="col-lg-6 cold-md-6 mb-4">
      <div className="card-base mb-3 shadow h-100">
        <img src={url} className="card-img-top" alt="" />
        <div className="card-body-base">
          <h5 className="card-title-base mb-auto">{name}</h5>
        </div>
      </div>
    </div>
  );
};

export default BaseCocktailCard;
