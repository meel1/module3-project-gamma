import { useSelector } from "react-redux";
import { useGetCocktailsByIngredientQuery } from "./store/apiSlice";
import { chunk } from "lodash";
import { useState } from "react";
import CocktailCard from "./CocktailCard";
import AlcoholDropdown from "./AlcoholDropdown";


const AlcoholSearch = () => {
  const selectedAlcohol = useSelector((state) => state.filter.value);
  const { data, isLoading } = useGetCocktailsByIngredientQuery(selectedAlcohol);
  const [currentPage, setCurrentPage] = useState(0);
  const chunks = chunk(data, 12);

  if (isLoading) return <div>Loading...</div>;

  if (!data || data.length === 0 || data === null) {
    return <AlcoholDropdown/>
  } else {
    return (
      <div className="columns is-centered">
        <h1 className="text-light">Cocktails with: {selectedAlcohol}</h1>
        <div className="column is-narrow">
          <div className="container">
            <div className="row mb-4">
              {chunks[currentPage].map((cocktail) => (
                <div key={cocktail.id} className="col-3 mb-4">
                  <CocktailCard

                    name={cocktail.name}
                    url={cocktail.url}
                  />
                </div>
              ))}
            </div>
            <div className="pagination-container">
              <button type="button" className="btn btn-outline-light"
                onClick={() => setCurrentPage(currentPage - 1)}
                disabled={currentPage === 0}
              >
                Prev
              </button >
              <button type="button" className="btn btn-outline-light"
              >
                {currentPage + 1}
              </button>
              <button type="button" className="btn btn-outline-light"
                onClick={() => setCurrentPage(currentPage + 1)}
                disabled={currentPage === chunks.length - 1}
              >
                Next
              </button>
            </div>
          </div>
        </div>
      </div>
    );
  }
}


export default AlcoholSearch;
