import "./App.css";
import { Outlet, useLocation } from "react-router-dom";
import Nav from "./Nav";

const App = () => {
  const location = useLocation();

  return (
    <div className="container">
      {location.pathname !== "/" && <Nav />}
      <div className="mt-1">
        <Outlet />
      </div>
    </div>
  );
};

export default App;
