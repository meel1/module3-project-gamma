import { useSignupMutation } from "./store/apiSlice";
import { useState, useEffect } from "react";
import { useNavigate } from "react-router-dom";
import AlertError from "./AlertError";

const SignUp = () => {
  const navigate = useNavigate();
  const [signup, signupResult] = useSignupMutation();
  const [errorMessage, setErrorMessage] = useState("");
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [full_name, setName] = useState("");
  const [email, setEmail] = useState("");
  const [passwordConfirmation, setPasswordConfirmation] = useState("");

  useEffect(() => {
    if (signupResult.error) {
      if (signupResult.error.status === 400) {
        setErrorMessage(signupResult.error.data.detail);
      }
      if (signupResult.error.status === 422) {
        setErrorMessage(signupResult.error.data.detail);
      }
    }
    if (signupResult.isSuccess) navigate("/user")
  }, [signupResult, navigate])

  const handleSubmit = (e) => {
    e.preventDefault();
    if (password !== passwordConfirmation) {
      setErrorMessage("Password does not match confirmation");
      return;
    }
    signup({ username, password, full_name, email });
  };
  return (
    <div className="row nav-padding">
      <div className="col-md-6 offset-md-3">
        <h1 className="text-light">SignUp</h1>
        <form onSubmit={handleSubmit}>
          {errorMessage && <AlertError>{errorMessage}</AlertError>}
          <div className="mb-3">
            <label htmlFor="name" className="form-label text-light">
              Name
            </label>
            <input
              type="text"
              className="form-control"
              id="full-name"
              value={full_name}
              placeholder="ie. John Doe"
              onChange={(e) => setName(e.target.value)}
            />
          </div>
          <div className="mb-3">
            <label htmlFor="email" className="form-label text-light">
              Email
            </label>
            <input
              type="email"
              className="form-control"
              id="exampleInputEmail1"
              placeholder="ie. johndoe@email.com"
              aria-describedby="emailHelp"
              value={email}
              onChange={(e) => setEmail(e.target.value)}
            />
            <small id="emailHelp" className="form-text text-muted">We'll never share your email with anyone else.</small>
          </div>
          <div className="mb-3">
            <label htmlFor="SignUp__username" className="form-label text-light">
              Username
            </label>
            <input
              type="text"
              className="form-control"
              id="SignUp__username"
              placeholder="Provide a unique username"
              value={username}
              onChange={(e) => setUsername(e.target.value)}
            />
          </div>
          <div className="mb-3">
            <label htmlFor="SignUp__password" className="form-label text-light">
              Password
            </label>
            <input
              type="password"
              className="form-control"
              id="SignUp__password"
              placeholder="Type your secure password"
              onChange={(e) => {
                setPassword(e.target.value);
                setErrorMessage("");
              }}
            />
          </div>
          <div className="mb-3">
            <label
              htmlFor="SignUp__password_confirmation"
              className="form-label text-light"
            >
              Confirm Password
            </label>
            <input
              type="password"
              className="form-control"
              id="SignUp__password_confirmation"
              value={passwordConfirmation}
              placeholder="Retype your secure password"
              onChange={(e) => {
                setPasswordConfirmation(e.target.value);
                setErrorMessage("");
              }}
            />
          </div>
          <button type="submit" className="btn btn-outline-light">
            Submit
          </button>
        </form>
      </div>
    </div>
  );
};

export default SignUp;
