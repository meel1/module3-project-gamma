import { selected } from "./store/filterSlice";
import { useDispatch } from "react-redux";
import { useNavigate } from "react-router-dom"

const AlcoholDropdown = () => {
    const navigate = useNavigate()
    const dispatch = useDispatch();
    const handleIngredientChange = (event) => {
    const spiritChoice = event.target.value;
        dispatch(selected(spiritChoice));
    }
    const handleSubmit = (e) => {
        e.preventDefault();
        navigate("/api/cocktails/alcohol/results")
    }
  return (
    <>
        <select onChange={handleIngredientChange} className="form-select form-select-lg mb-3" aria-label=".form-select-lg example">
            <option >Choose a spirit</option>
            <option value="Brandy">Brandy</option>
            <option value="Gin">Gin</option>
            <option value="Rum">Rum</option>
            <option value="Tequila">Tequila</option>
            <option value="Vodka">Vodka</option>
            <option value="Whiskey">Whiskey</option>
        </select>
        <div className="d-grid gap-2 col-6 mx-auto">
            <button type="button" className="btn btn-warning btn-lg" onClick={handleSubmit}>Search!</button>
        </div>
    </>
  );
};

export default AlcoholDropdown;
