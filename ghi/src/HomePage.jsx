import React from 'react';
import { useGetTenRandomCocktailsQuery } from "./store/apiSlice";
import BaseCocktailCard from './BaseCocktailCard';
import { Link } from 'react-router-dom';

function HomePage() {
  const { data: cocktails, error, isLoading } = useGetTenRandomCocktailsQuery();

  if (isLoading) return <div>Loading...</div>;
  if (error) return <div>Error: {error.message}</div>;

  const fourCocktails = cocktails.slice(0, 4);

  return (
    <div className="px-4 py-5 my-5 text-center">
      <h1 className="display-5 fw-bold text-light">Mix Masters</h1>
      <div className="col-lg-6 mx-auto">
        <p className="lead mb-4 text-light">
          The premiere solution for cocktail enthusiasts!
        </p>
        <div className="row">
          {fourCocktails.map((cocktail) => (
            <BaseCocktailCard
              key={cocktail.id}
              name={cocktail.name}
              url={cocktail.url}
            />
          ))}
        </div>
        <div>
            <p className="text-light"> Unleash your inner Alcoholic!</p>
        </div>
        <div>
            <div>
                <Link to='/login' className='btn btn-light btn-lg' style={{ marginRight: '20px' }}>Login</Link>
                <Link to='/signup' className='btn btn-light btn-lg'>Signup</Link>
            </div>
        </div>
      </div>
    </div>
  );
}

export default HomePage;
