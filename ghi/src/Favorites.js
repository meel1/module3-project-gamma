import { useGetFavoritesQuery } from "./store/apiSlice";
import FavoritesCard from "./FavoritesCard";

const Favorites = () => {
    const { data, isLoading } = useGetFavoritesQuery();

    if (isLoading) return <div>Loading...!</div>;

    return (
        <div className="container-lg">
            <div className="row">
                {data.map((cocktail) => (
                    <div key={cocktail.id} className="col-lg-3 col-md-6 col-sm-12 mb-4">
                        <FavoritesCard id={cocktail.id} />
                    </div>
                ))}
            </div>
        </div>
    );
};

export default Favorites;
