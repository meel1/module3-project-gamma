import React from "react";
import ReactDOM from "react-dom/client";
import "./index.css";
import App from "./App";
import reportWebVitals from "./reportWebVitals";
import { store } from "./store/store";
import { Provider } from "react-redux";
import { createBrowserRouter, RouterProvider } from "react-router-dom";
import UserHome from "./UserPage";
import ErrorNotification from "./ErrorNotification";
import Login from "./Login";
import SignUp from "./SignUp";
import CocktailDetails from "./CocktailDetail";
import Favorites from "./Favorites";
import HomePage from "./HomePage";
import About from './About';
import IngredientFilter from "./IngredientFilter";
import AlcoholFilter from "./AlcoholFilter";
import AlcoholSearchResults from "./AlcoholSearchResults";
import NameFilter from "./NameFilter";


const domain = /https:\/\/[^/]+/
const basename = process.env.PUBLIC_URL.replace(domain, '')
const router = createBrowserRouter([
  {
    element: <App />,
    errorElement: <ErrorNotification />,
    children: [
      {
        path: "/",
        element: <HomePage />,
      },
      {
        path: "/user",
        element: <UserHome />,
      },
      {
        path: "/about",
        element: <About />,
      },
      {
        path: "/login",
        element: <Login />,
      },
      {
        path: "/signup",
        element: <SignUp />,
      },
      {
        path: "/favorites",
        element: <Favorites />,
      },
      {
        path: "/api/cocktails/name",
        element: <NameFilter />,
      },
      {
        path: "/api/cocktails/ingredient",
        element: <IngredientFilter />,
      },
      {
        path: "/api/cocktails/alcohol",
        element: <AlcoholFilter />,
      },
      {
        path: "/api/cocktails/alcohol/results",
        element: <AlcoholSearchResults />,
      },
      {
        path: "/api/cocktails/name/:name",
        element: <CocktailDetails />,
      },
    ],
  },
], {basename});

const root = ReactDOM.createRoot(document.getElementById("root"));
root.render(
  <React.StrictMode>
    <Provider store={store}>
      <RouterProvider router={router} />
    </Provider>
  </React.StrictMode>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
