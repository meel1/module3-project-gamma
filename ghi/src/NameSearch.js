import { useState, useEffect } from 'react';
import { reset, filter } from "./store/searchSlice";
import { useDispatch } from 'react-redux';

const NameSearch = () => {
    const [searchCriteria, setSearchCriteria] = useState('');
    const dispatch = useDispatch()

    useEffect(() => {
        setSearchCriteria('')
    }, [])

    const handleSubmit = (e) => {
        e.preventDefault();
        dispatch(filter(searchCriteria));
    }
    return (
        <form className="row" onSubmit={handleSubmit}>
            <div className='col'>
                <input
                    className='form-control form-control-lg'
                    type="text"
                    placeholder='cocktail name'
                    value={searchCriteria}
                    onChange={(e) => setSearchCriteria(e.target.value)}
                />
            </div>
            <div className="col">
                <button className='btn btn-lg text-light' type='submit'>Search</button>
                <button className='btn btn-lg text-light'
                    type='button'
                    onClick={() => {
                        dispatch(reset())
                        setSearchCriteria('');
                    }}
                >
                    Reset
                </button>
            </div>
        </form>
    )
}

export default NameSearch;
