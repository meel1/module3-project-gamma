import {
  useGetFavoritesQuery,
  useCreateFavoriteMutation,
  useDeleteFavoriteMutation,
} from "./store/apiSlice";
import { useEffect, useState } from "react";

const FavoriteButtons = (props) => {
  const [favorite, setFavorite] = useState(null);
  const { data: favorites } = useGetFavoritesQuery();
  const [ deleteFavorite ] = useDeleteFavoriteMutation();
  const [ createFavorite ] = useCreateFavoriteMutation();

  useEffect(() => {
    if (favorites) {
      setFavorite(favorites.find((f) => f.id  === props.id) || null);
    }
  }, [favorites, props.id]);
  return (
    <>
      {!favorite && (
        <button
          className="btn btn-success"
          onClick={() => createFavorite({id: props.id})}
        >
          Favorite
        </button>
      )}
      {favorite && (
        <button
          className="btn btn-danger"
          onClick={() => deleteFavorite(favorite.id)}
        >
          Unfavorite
        </button>
      )}
    </>
  );
};

export default FavoriteButtons;
