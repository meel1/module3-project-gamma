import { Link, NavLink, useNavigate } from "react-router-dom";
import { useGetAccountQuery, useLogoutMutation } from "./store/apiSlice";
import "./nav.css";
import cocktailIcon from "./images/cocktail-icon.png";

const Nav = () => {
  const { data: account } = useGetAccountQuery();
  const [logout] = useLogoutMutation();
  const navigate = useNavigate();
  const handleLogout = () => {
    logout();
    navigate("/");
  };

  return (
    <nav className="navbar navbar-expand-lg custom-navbar">
      <div className="container-fluid">
        <Link to={account? ("/user"):("/")} className="navbar-brand text-light">
          <img src={cocktailIcon} alt="Cocktails" height="40" />
        </Link>
        <button
          className="navbar-toggler"
          type="button"
          data-bs-toggle="collapse"
          data-bs-target="#navbarNav"
          aria-controls="navbarNav"
          aria-expanded="false"
          aria-label="Toggle navigation"
        >
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarNav">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">
            <li className="nav-item">
              <NavLink to={"/about"} className={"nav-link text-light"}>
                About
              </NavLink>
            </li>
            {account && (
            <li className="nav-item dropdown">
              <NavLink
                className="nav-link dropdown-toggle text-light"
                id="navbarDropdownMenuLink"
                role="button"
                data-bs-toggle="dropdown"
                aria-expanded="false"
              >
                Browse Cocktails
              </NavLink>
              <ul
                className="dropdown-menu"
                aria-labelledby="navbarDropdownMenuLink"
              >
                <h6 className="dropdown-header">Search By...</h6>
                {account && (
                <li>
                  <NavLink to={"/api/cocktails/name"} className={"nav-link"}>
                    Name
                  </NavLink>
                </li>
                )}
                {account && (
                  <li className="nav-item">
                    <NavLink
                      to={"/api/cocktails/ingredient"}
                      className={"nav-link"}
                    >
                      Ingredient
                    </NavLink>
                  </li>
                )}
                {account && (
                  <li className="nav-item">
                    <NavLink
                      to={"/api/cocktails/alcohol"}
                      className={"nav-link"}
                    >
                      Alcohol
                    </NavLink>
                  </li>
                )}
              </ul>
            </li>
            )}
            {account && (
              <li className="nav-item">
                <NavLink to={"/favorites"} className={"nav-link text-light"}>
                  Favorites
                </NavLink>
              </li>
            )}
            {!account && (
              <li className="nav-item">
                <NavLink to={"/login"} className={"nav-link text-light"}>
                  Login
                </NavLink>
              </li>
            )}
            {!account && (
              <li className="nav-item">
                <NavLink to={"/signup"} className={"nav-link text-light"}>
                  Sign Up
                </NavLink>
              </li>
            )}
          </ul>
          {account && (
            <button
              className="btn btn-outline-danger text-light"
              onClick={handleLogout}
            >
              Logout
            </button>
          )}
        </div>
      </div>
    </nav>
  );
};

export default Nav;
