import "./about.css";
const About = () => {
  return (
    <div className="bg-light py-5">
      <div className="container py-5 background-about">
        <div className="row mb-4">
          <div className="col-lg-5">
            <h2 className="display-4 font-weight-light text-center">
              Main Mix Masters
            </h2>
            <p className="font-italic text-center">
              Mix Masters is a great tool to find your next cocktail or mocktail!
              Get to know the brains behind it all!
            </p>
          </div>
        </div>

        <div className="row text-center">
          <div className="col-xl-3 col-sm-6 mb-5">
            <div className="bg-white rounded shadow-sm py-5 px-4">
              <img
                src="https://media.istockphoto.com/id/1398824694/vector/portrait-of-a-young-beautiful-woman-with-long-brown-hair-and-in-pink-headphones.jpg?s=612x612&w=0&k=20&c=nB9vywv-PtKWgKscfjFmg34c8bVpxnoFVvk81HRPsMA="
                alt=""
                width="100"
                className="img-fluid rounded-circle mb-3 img-thumbnail shadow-sm"
              />
              <h5 className="mb-0">Lavanya Nese</h5>
              <span className="small text-uppercase text-muted">
                CEO - Founder
              </span>
              <ul className="social mb-0 list-inline mt-3">
                <li className="list-inline-item">
                  Favorite cocktail: Electric Lemonade
                </li>
              </ul>
            </div>
          </div>
          <div className="col-xl-3 col-sm-6 mb-5">
            <div className="bg-white rounded shadow-sm py-5 px-4">
              <img
                src="https://bootstrapious.com/i/snippets/sn-about/avatar-3.png"
                alt=""
                width="100"
                className="img-fluid rounded-circle mb-3 img-thumbnail shadow-sm"
              />
              <h5 className="mb-0">Eric Chi</h5>
              <span className="small text-uppercase text-muted">
                CEO - Founder
              </span>
              <ul className="social mb-0 list-inline mt-3">
                <li className="list-inline-item">
                  Favorite cocktail: French 75
                </li>
              </ul>
            </div>
          </div>
          <div className="col-xl-3 col-sm-6 mb-5">
            <div className="bg-white rounded shadow-sm py-5 px-4">
              <img
                src="https://bootstrapious.com/i/snippets/sn-about/avatar-2.png"
                alt=""
                width="100"
                className="img-fluid rounded-circle mb-3 img-thumbnail shadow-sm"
              />
              <h5 className="mb-0">Mitch Cain</h5>
              <span className="small text-uppercase text-muted">
                CEO - Founder
              </span>
              <ul className="social mb-0 list-inline mt-3">
                <li className="list-inline-item">
                  Favorite cocktail: Old Fashioned
                </li>
              </ul>
            </div>
          </div>
          <div className="col-xl-3 col-sm-6 mb-5">
            <div className="bg-white rounded shadow-sm py-5 px-4">
              <img
                src="https://bootstrapious.com/i/snippets/sn-about/avatar-1.png"
                alt=""
                width="100"
                className="img-fluid rounded-circle mb-3 img-thumbnail shadow-sm"
              />
              <h5 className="mb-0">Erick Emelio</h5>
              <span className="small text-uppercase text-muted">
                CEO - Founder
              </span>
              <ul className="social mb-0 list-inline mt-3">
                <li className="list-inline-item">
                  Favorite cocktail: Vodka Soda
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default About;
