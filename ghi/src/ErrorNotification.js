function ErrorNotification(props) {
    if (!props.error) {
        return null;
    }

    return (
        <div className="notification is-danger text-light">
            {props.error}
        </div>
    );
}

export default ErrorNotification;
