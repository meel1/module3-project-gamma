import ErrorNotification from "./ErrorNotification";
import { useGetAllCocktailsQuery } from "./store/apiSlice";
import { useSelector } from "react-redux";
import { chunk } from "lodash";
import { useState } from "react";
import CocktailCard from "./CocktailCard";


const CocktailsList = () => {
  const searchCriteria = useSelector((state) => state.search.value);
  const { data, isLoading } = useGetAllCocktailsQuery();
  const [currentPage, setCurrentPage] = useState(0);

  const filteredCocktails = () => {
    if (searchCriteria) {
      return data.filter((cocktail) =>
        cocktail.name.toLowerCase().includes(searchCriteria.toLowerCase())
      );
    } else {
      return data;
    }
  };

  const chunks = chunk(filteredCocktails(), 12);

  if (isLoading) return <div>Loading...</div>;

  if (!isLoading && filteredCocktails().length === 0) {
    return <ErrorNotification error="No matching cocktails found." />;
  }

  return (
    <div className="columns is-centered">
      <div className="column is-narrow">
        <div className="container-lg">
          <div className="row">
            {chunks[currentPage].map((cocktail) => (
              <div key={cocktail.id} className="col-lg-3 col-md-6 col-sm-12 mb-4">
                <CocktailCard
                  name={cocktail.name}
                  url={cocktail.url}
                />
              </div>
            ))}
          </div>
          <div className="pagination-container mt-4">
            <button
              type="button"
              className="btn btn-outline-light mr-2"
              onClick={() => setCurrentPage(currentPage - 1)}
              disabled={currentPage === 0}
            >
              Prev
            </button>
            <button
              type="button"
              className="btn btn-outline-light mr-2"
            >
              {currentPage + 1}
            </button>
            <button
              type="button"
              className="btn btn-outline-light"
              onClick={() => setCurrentPage(currentPage + 1)}
              disabled={currentPage === chunks.length - 1}
            >
              Next
            </button>
          </div>
        </div>
      </div>
    </div>
  );
};

export default CocktailsList;
