import { useParams } from "react-router";
import { useGetCocktailByNameQuery, useGetAccountQuery } from "./store/apiSlice";
import CocktailDetailCard from "./CocktailDetailCard";

const CocktailDetails = (props) => {
  const { name, id } = useParams();
  const { data: account } = useGetAccountQuery();
  const { data, isLoading } = useGetCocktailByNameQuery(name);

  if (isLoading) return <div>Loading...</div>;

  return (
    <main className="nav-padding">
    <CocktailDetailCard data={data} account={account} id={id} />
    </main>
  );
};
export default CocktailDetails;
