import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react";

export const cocktailsApi = createApi({
  reducerPath: "cocktailsApi",
  baseQuery: fetchBaseQuery({
    baseUrl: process.env.REACT_APP_API_HOST,
  }),
  endpoints: (builder) => ({
    getAllCocktails: builder.query({
      query: () => ({
        url: "/api/cocktails",
        credentials: "include",
      }),
      transformResponse: (response) => response.cocktails

    }),
    getAllIngredients: builder.query({
      query: () => ({
        url: "/api/ingredients",
      }),
    }),
    getCocktailByName: builder.query({
      query: (name) => ({
        url: `/api/cocktails/name/${name}`,
        credentials: "include",
      }),
    }),
    getCocktailsByIngredient: builder.query({
      query: (ingredient) => ({
        url: `/api/cocktails/ingredient/${ingredient}`,
        credentials: "include",
      }),
      transformResponse: (response) => response.cocktails,
    }),
    getCocktailById: builder.query({
      query: (id) => ({
        url: `/api/cocktails/${id}`,
        credentials: "include",
      }),
    }),
    getTenRandomCocktails: builder.query({
      query: () => ({
        url: `/api/random/cocktails`,
      }),
    }),
    getOneRandomCocktail: builder.query({
      query: () => ({
        url: `/api/random/cocktail`,
        credentials: "include",

      }),
    }),
    getAccount: builder.query({
      query: () => ({
        url: `/token`,
        credentials: "include",
      }),
      transformResponse: (response) => (response ? response.account : null),
      providesTags: ["Account"],
    }),
    logout: builder.mutation({
      query: () => ({
        url: "/token",
        method: "DELETE",
        credentials: "include",
      }),
      invalidatesTags: ["Account", "Favorites", "Search"],
    }),
    login: builder.mutation({
      query: ({ username, password }) => {
        const body = new FormData();
        body.append("username", username);
        body.append("password", password);
        return {
          url: `/token`,
          method: `POST`,
          body,
          credentials: "include",
        };
      },
      invalidatesTags: ["Account"],
    }),
    signup: builder.mutation({
      query: (body) => ({
        url: `/api/accounts`,
        method: "POST",
        body,
        credentials: "include",
      }),
      invalidatesTags: ["Account"],
    }),
    getFavorites: builder.query({
      query: () => ({
        url: `/api/favorites/mine`,
        credentials: "include",
      }),
      transformResponse: (response) => response.favorites,
      providesTags: ["Favorites"],
    }),
    createFavorite: builder.mutation({
      query: (body) => ({
        url: `/api/favorites`,
        method: "POST",
        body,
        credentials: "include",
      }),
      invalidatesTags: ["Favorites"],
    }),
    deleteFavorite: builder.mutation({
      query: (id) => ({
        url: `/api/favorites/${id}`,
        method: "DELETE",
        credentials: "include",
      }),
      invalidatesTags: ["Favorites"],
    }),
  }),
});

export const {
  useGetAllIngredientsQuery,
  useGetFavoritesQuery,
  useGetAllCocktailsQuery,
  useGetCocktailByNameQuery,
  useGetAccountQuery,
  useLogoutMutation,
  useLoginMutation,
  useSignupMutation,
  useCreateFavoriteMutation,
  useDeleteFavoriteMutation,
  useGetCocktailByIdQuery,
  useGetTenRandomCocktailsQuery,
  useGetOneRandomCocktailQuery,
  useGetCocktailsByIngredientQuery,
} = cocktailsApi;
