import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  value: "Rum",
};

export const filterSlice = createSlice({
  name: "filter",
  initialState,
  reducers: {
    selected: (state, action) => {
      state.value = action.payload;
    },
    resetIngredientSearch: (state, action) => {
      state.value = "Rum";
    }
  },
});

export const { selected, resetIngredientSearch } = filterSlice.actions;

export default filterSlice.reducer;
