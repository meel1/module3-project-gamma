import { configureStore } from "@reduxjs/toolkit";
import { setupListeners } from "@reduxjs/toolkit/query";
import { cocktailsApi } from "./apiSlice";
import searchReducer from "./searchSlice";
import filterReducer from "./filterSlice";

export const store = configureStore({
  reducer: {
    filter: filterReducer,
    search: searchReducer,
    [cocktailsApi.reducerPath]: cocktailsApi.reducer,
  },
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware().concat(cocktailsApi.middleware),
});

setupListeners(store.dispatch);
