6/26/2023 - Day 1

Today is the start of the project.
We are planning to clone our repo and and start to work on the authentication feature for user login and logout.
Currently we have no blockers

6/27/2023 - Day 2

We cloned the repo yesterday and started working on the authentication part of the project. My group and I did a quick recap of what the wireframing decisions were to prepare ourselves for the overall goal of the project. We were able to start up the project on docker and followed along with demonstrations from class and from the videos to guide us through the set up for the authentication functionality with JWTdown for FastAPI.

We ran into a blocker when trying to complete the create and get functions in the queries accounts.py. We were able to get the database to save the account when created, but we kept running into a 401 error "Invalid username or password". After several hours of debugging, we were able to get the token to return back and 200 status on FastAPI. We were happy with that and ended our day there.

Today we are going to work on our user stories and to complete the functionality of the authentication. As of right now there is no area for an email to be saved to the db, and usernames/passwords are not unique and will be saved regardless of repeating entries. We will continue with fixing that issue, and learn more about MongoDB. If we get finished with those goals early we will start working on other features of the project.

6/28/2023 - Day 3

Yesterday we finished up with the authentication functionality and made the username a unique property and can not be recreated. We started to work on making calls to the cocktail api and routing them on FastAPI to make calls. We also created an env file to make our api key hidden from git.

We ran into a blocker where the API key was not being called properly and was running an error in our get list of cocktails. We also need to remove the string password from our account before it is saved in the database.

Today we are going to implement changes to the auth just to delete the string password and keep the hashed password. Also we are going to work on finishing up routes and queries for a favorites list and for cocktails to be pulled from the database

6/29/2023 - Day 4

Yesterday we finished with cleaning up the authentication to delete the string password and only save the hashed password into the db. We also wrote a skeleton of the favorites list functionality and a few more endpoints for the cocktails

Blockers: we were having trouble with organization of our code, and we are realizing our API has some data that needs cleaning

Today we are going to work on sanitizing the api data for our data, continuing to work on the favorites functionatlity and adding more endpoints for the fastapi.

6/30/2023 - Day 5

We finished with the major portions of the backend and have functioning code that is calling to the api for the endpoints we need. We also were finishing up with the favorites functionality which is still having some issues returning all the information we want

Blockers: we are able to print out the account_id and the id of the drink but not any additional info for the favorites functionality and the api key still has not been implemented properly so that we can get the full library when making calls to the api.

Today we are going to start on the front end and make the basic pages we need and start implementing some features, while fixing up the favorite functionality.

7/10/23 - Day 6

We finished with the backend and pivoted our cocktail app to contain more than just alcoholic drinks. we took a week off for summer break. we were not able to work on the front end because of some issues with our api and the pivot to the new goal.

We still need to make an acls file to make the queries functions look cleaner, but for the time being it is working as intended

Today we are starting on the front end.

7/11/23 - Day 7

We ran into an issue after trying to do the front end with redux. We tried to follow along with the video examples but when installing the redux toolkit I tried to do an audit fix to solve some of the vulnerabilities that the terminal was warning about. Afterwards the localhost:3000 page was no longer loading and an error on the docker terminal read "error:0308010c:digital envelope routines::unsupported". All attempts to reset the containers failed. This problem we worked on for several hours and eventually settled with reverting back to the main branch and starting over.

The blocker was this error which spent our whole day, but I was also able to start working with the front-end using react only and a barebones webpage is now visible. I will continue to work with react only and try to convert the work into redux after learning more about it.

We will continue to work on the front-end today and try to create a product ready main page.

7/12/23 - Day 8

We were successful in starting the front end and the redux paradigm is starting to make more sense as we are repeating our steps from day 6. We were able to get a barebones nav bar that has login, signup, and logout capabilities as well as a list of all drinks with the name, id, and picture in each row. We also have the main logo bring you back to the home page. We were also able to get bootstrap to start working, which was two lines of code added into the index.html in the public folder.

There were some blockers regarding how to implement bootstrap and what code needs to be inside the apiSlice.js file o allow for the list to be shown only when a user is logged in. It ended up being this:

  endpoints: (builder) => ({
    getAllCocktails: builder.query({
      query: () => ({
        url: '/api/cocktails',
        credentials:'include',
    }),
    }),
  })

  the crednetials:'include' was the crutial line that we were missing, but once added in the errors went away.

  Now we are a bit more confident on implementing more features and will try to format the home page to have the cocktail list to be in separate cards not a table. Also we will try to start on making a favorites page.

7/13/2023 - Day 9

The home page with the list of drink cards is set up and the boot strap is set up to link the user to the details page when they click on the details button. My other group members were able to set up the code to create a details page, but currently we are unsuccessful in linking the page to the cards which we will be focusing on today. We also changed the models and the queries cocktail.py to have the output to have more readable json. We changed all instances of drink to name, thumbnail to url, and instruction_measurements to ingredient_measurements.

We were having a blocker with a partner's env file not reading the REACT_APP_API_HOST which was resolved after some debugging that she only need to replace those instances with 'http://localhost:8000" which we will be aware of in the future.

Today we are going to figure out the route to the details page and create a main page that all users will initially see without login. We may also try to attempt the favorites page.

7/14/2023 - day 10

As a group we were able to link up the details page with the list of drink cards and we briefly formatted the detail page so that it displays an image, the instructions, ingredients, and a post date. We split up the work yesterday to work on search by name feature and the favorites feature.

The blocker we met was the search function would not load up the images of the cards when a user enters in the name of a drink, also it may not list out all the drinks with that partial name in it ie Margarita should come up with multiple drinks of there are multiple drinks with the name margarita in them (blue margarita, tommy's margarita, etc.). Furthermore, we were met with an issue with the favorites page and were finally able to get the routes to match and show the cards on the favorites page, but the information is only pulling from the data that is in the mongodb which only contains the id and the account id without any of the name, image url so that it preventing the user from seeing anything useful.

Today we will try to figure out how to fetch the data from the search by name route or search by id route to pull the data into the favorites page. If we get through with that we may try to fix the search by name feature as well, but we are trying just to have the favorites page completed by end of today.


7/17/2023 - Day 11

Last week we worked on trying to get the favorites page to work and were able to get the cards to show up on the favorites page, but no information. We are still attempting another method of getting the information from the get cocktail by name to pull the details from there. We were able to get search by name function complete on the front end for filtering through the home page list of drinks.

The blocker is still the issue with the favorites page, we are having trouble with using the hashed id of the favorited item and pulling the details of the drink into the favorites page. We will try to go another way with creating a favorites card and import the information that way instead, but it is still a working idea.

Today we will try to work on the favorites page again. We had our fourth member return so we will catch him up on what has changed on the app since he's been away. We will also try to get the unit tests done by today, time permitting.


7/18/2023 - Day 12

Yesterday we were able to finish up with the favorites feature, and another team member finished setting up a page for non-logged in users, the non-logged in users are also able to see four randomly generated cocktails and a login and sign up button. We merged all these new features and the name search feature to main

Yesterday we noticed that the login and sign up was not reloading the favorites page propery and the cached favorites list appears first when you switch users and click on the favorites list. it isn't until you favorite or unfavorite something that the list changes to the users actual list. Furthermore, the routing of the signup and login buttons need to be readdressed because it is not consistent on all the pages.

Today we will start and hopefully complete the unit tests and clean up some of the routes and fix the favorites page caching issue.


7/19/2023 - Day 13

We each completed a unit test and worked on a few together as we had errors in some of the tests, but they are all passing now. We fixed up the login error message if there was an incorrect password. We also worked on getting the sign up form to link you to the login form once you create a new account, and if you logout the page automatically goes to the nonlogged in mainpage.

We had some slight confusion with the backend unit tests and we ended up changing up the list all drinks function to return a dictionary with a key "cocktails" and an array of dictionaries with the data instead just an array of dictionaries.

Today we are going to work on the sign up form as it is not saving to mongodb and we will fix the favorites page so that if you switch accounts the favorites page will update automatically. We will also try to get some more bootstrapping and themes placed in the mainpage and throughout the website. We are talking about design today and found some themes online that we liked. We are going to implement a pagination on the home page for logged in users and limit the number of cocktails that are loaded on the initial page to improve load times. Furthermore, we want to implement a search by ingredient page, and a about page.


7/20/2023 - Day 14

Yesterday we were able to get the login, signup, favorites all working properly. We had to use dispatch('') to reset the search bar every time we logged in to avoid it from not refreshing with a new user. We were also able to make it so that between users the favorites list would reset by invalidating Favorites tag when the user logged out. Currently all the features we have are working as intended. We started on beautifying the project and now there is pagination and only 20 cards are being shown to the user at a time by using lodash chunk. We also started working on the search by ingredients feature where we will be using a drop down menu to allow the user to choose an item to search the cocktails by.

Currently we are able to get the dropdown and save the state so that it pulls the name of the ingredient, but we are running into a issue with taking that data and adding that into the apislice to search by ingredients. However we noticed that the search by ingredients apislice is working so we just have to link up the two pieces.

Today we will finish up with the search by ingredients and continue to make the page look better with bootstrap. We are starting to work on design choices and considering to create an abouts page and maybe a few more features down the line, but want to get the ingredients search set first.


7/24/2023 - Day 15

Last week we got the ingredients search to work but there are a few issues we are still having similar to with the name search feature. The search is not refreshing when a new user logs in and if you select the default value it does not work as expected. We are not splitting out work up to make other areas of the project look better.

The blocker we are having with the ingredients search is trying to find the right place to useDispatch to refresh the ingredients search. Furthermore, we are trying to make a better design choice for the search functions so we will be trying tailwind today

Hopefully we will have tailwind downloaded into the project and implement some stylistic changes to the pages we have. Furthermore, the ingredients feature will be working properly.


7/25/2023 - Day 16

The ingredients search is now pulling the information from the api for the ingredients listed in the drop down menu. We are having issues with some ingredients in the list are not actual ingredients in the drink search by ingredients api so it leads to a blank page We need to correct that today. The front end continues to get a make over and it looking better with the help of my teammates.

Blockers yesterday was the unit testing was failing when creating the new route and query, but that was resolved before the end of day and the ingredient search function is working now. Tailwind was also an issue because have been using boot strap all this time already it was not responding well to what we already had so we are pivoting away from tailwind at this time.

After figuring out how to resolve the blank page issue, we need to reset the users search after login so that the search function does not retain the last person's search. Furthermore, we need to consider other ways to search by alcohol and potentially a multi search function. We will also need to make a new search by drop down on the nav bar to contain all of these search methods.


7/26/2023 - Day 17

I set up some edge cases for the ingredients search and now it does not send the user to a blank page, and it works okay, but we still have some trouble with reseting the search page. My teammates were able to set up a ratings system, but it is not currently being saved to the card so it changes on refresh. Furthermore the css on the pages are looking good but there are a lot more changes needed for it to look consistent

I was having some issues with the ingredients search trying to get the page to reset on login, but I was able to get that working with a dispatch on the login page.

I will start working on the search by alcohol page, and my teammates will be working on setting up a rating system for the details page. I am still thinking about a multisearch function but we may do that as a stretch goal. We still need to create a new search drop down on the nav bar to contain all of the search methods.


7/27/2023 - Day 18

Yesterday I stopped working on the ingredients page after getting it to properly refresh on login, and started working on the search by alcohol. The search by alcohol page works and it is a drop down with a submit button that sends the user to another page for the results, but after searching by alcohol the search by ingredients page also displays that result. May have to redesign the page in order to make it more intuitive for the user. I changed some of the file naming around to make an account page which is the first thing the user will see when they login, and a random cocktail is generated on the page with a welcome message. At the end of the day I started up on a comments feature. My teammates were working on fixing the rating system for the detail pages, and making all the css on all the pages look consistent and neat.

Some blockers on search by alcohol were trying to get the user to navigate to a new page, but useNavigate worked great. For the userpage where were some issues, but the solution was relatively easy. I felt the back end for the comments section was a bit confusing, but I will get back on it today.

My teammates will continue to work on making the website look presentable, and finishing up the readme.md file as per the project requirements. We will also try to finish up the comments section today.


7/28/2023 - Day 19

Worked on the comments section and was able to get all the api routes to work. I still have to implement it to the front end and create unit tests. The website is looking good but we have a few css tweaks on a couple pages.

Blockers we noticed that the ingredients page was still throwing a cores error when entering the search page, and when searching either by ingredients or by alcohol the results remain on ingredients search between different searches. Furthermore, yesterday I was having some issue with the comments unit tests and will continue to try to complete them today.

We will continue to work on the issues and merge our final project and do a once over to make sure all the features are working properly. We will also complete with the readme and prepare our project for deployment.
