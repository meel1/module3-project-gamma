6/26/2023 - Working with Lavanya, Eric and Erick on a Cocktail web app called Mix Masters that allows you to search and faovirte cocktails and save them under a profile. Today we got the repo and started working on auth with Eric Chi as the driver. We ran into issues with creating a username/email that aligned with the MongoDB username and password form to sign in as a user once your username and password was created. Worked with SEIR David.

6/27/2023 - Working on User stories together as a team to fully flushout all of the features that we want to include in Mix Masters. We plan on going back to auth once we've completed User stories.

6/28/2023 - We completed Auth last night with Eric driving. We might have to go back and clean up some things we might have overlooked within auth. Added delete function for usernames. Eric Chi and I will be working on Cocktail endpoints while Erick and Lavanya are working on the favorite endpoints.

6/29/2023 - Contiued working on the cocktails routers and queries with Eric C while Lavanya and Erick E worked on favorites routers and queries. We've ran into a blocker with out API format and have decided wether or not to move forward with this API. Might have to implements ACLs to make the data more readable on our side as well with formatting some of the ingridients/measurement values into dictionaries easier for us to use.

7/3/2023 - 7/7/2023 - Off for summer break

7/10/2023 - 7/14/2023 - Out sick for the week

7/17/2023 - Getting caught up on what I've missed. The team has done great and got the Front End up and running for login/logout, listing cocktails and now working through the Favorites Page up and running. Created a Favorite Button and a FavoriteCard.js page

7/18/2023 - Working through Favorite Page, Home Page, and Login and Sign Up links. Ended up having a bug where the favorites would populate on whatever account we signed into. Also implementing unit tests per the rubric.

7/19/2023 - Created unit tests for cocktails and faovrites and passed all 9 tests having each memeber of the group complete 1-2 tests per the rubric. Implemented and sign in error when credentails dont match up and working on the auth sign in on the front end. Working to figure out how we want to style the page to make it look more presentable.

7/20/2023 - Worked on getting search by ingredient page up and running. Also redesigned the Cocktail Details page to be on its own card to make it easier to read and more pleasing to the eye. Continueing to work on how we want the app to look. Researching into theme ands wether we want to continue to work with bootstrap or look into tailwind for css.

7/24/2023 - The team resolved the issues we were having with search by ingredient. Still having some issues with ingredients that have no cocktails associated with them. Probably a result of the public api allowing a create function. Contiunue working on the front end aspect. Making the cards look cleaner and developing an overall theme for the site.

7/25/2023 - Working on a Star Rating system for your favorite cocktails that will be saved to the user. Running into a blockler with the implementation from an online source for the star rating system. I've installed the dependencies required for the project but its still not pulling those files and resulting in an error.

7/26/2023 - Got Star rating working for the details page so now youre able to click on a star and it saves it to the cocktail card. Worked on scaling and CSS for CocktailDetail Page. Resizing cocktail cards and margins so everything looks uniform.

7/27/2023 - Cocktail Detail page on the front end is up and running. Still working on sizing the Cocktail Cards throughout the different pages of the front end making small adjustments where needed. Realized after seeing Riley that the Star Rating system is only saving locally and that it will have to be implemented in the backend as well. Thought enitially that could all be done from the front end. Will have to work on that in stretch goal week.

7/28/2023 - Making sure that we have all deliverables up and running with out project. Making updates to the README.MD file while searching for bugs in the project. Merging main_page to main once everyhting is looked over. Main_page has the most up to date front end while main has the features all sorted out. Turning in project by end of day.
