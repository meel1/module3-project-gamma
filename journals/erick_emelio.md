D1 6/26
***
- * First day no prior tasks
- Started working on authentication for our project
- No blockers at the moment

D2 6/27
***
- Finished some components of our Auth
- Working on user stories / continue Auth development
- Ran into issues making a username unique

D3 6/28
***
Yesterday we continued working on our backend authentication as well as created user stories to map out some functionality for our project. We were able to resolve the issue involving a unique username when creating an account.
***
Today Lavanya and I are breaking off to work on user favorites. We added a cocktails.py router / queries file and 3 new models.(FavoriteIn, FavoriteOut, Favorites). After this I had to leave class early to catch a flight so tomorrow I will focus on testing the favorites feature.
***
Currently Lavaya and I have no blockers directly tied to user favorites but we need to test the functionality. I will also be focusing on catching up on the work the group did while I was gone.

D4 6/29



D5 6/30


D6 7/10
***


D7 7/11
***
Yesterday we started work on our front end using redux.
***
Today we worked on cleaning up some git workflow and branching issues to better organize our features and codebase. Lavanya and Eric worked through the redux videos while I continued on from Eric's barebones react version of the front-end as an alternative option to the redux approach. I was able to fix an issue with bootstrap styling not showing up and complete a login page / form / nav link. I ended up stashing these changes and regrouping with Eric and Lavanya towards the end of the day as I believe we will be able to move forward using redux.
***
There are no blockers as of now besides some minor issues on one of our machines, we will continue to build out the front-end tomorrow.

D8 7/12

D9 7/13

D10 7/14

D11 7/17

D12 7/18
***
Yesterday the group worked on fixing the favorites feature while I worked on so the main page for users who are not signed in. I was able to to include 4 random cocktail cards / login / logout buttons and removed the navbar as non-logged in users should not see it before signing up.
***
Today we are writing unittests and working on an issue with our favorites list cache not resetting upon switching accounts. I will be working on the CocktailbyID unittest and making some changes to our mainpage and navbar components.
***
I had some issues getting my unittests to pass but my group helped me debug before we signed off for the day. At the moment I have no blockers, will continue working on front-end components tomorrow.


D13 7/19
***
Yesterday we fixed our favorite feature / wrote unittests / worked on components.
***
Today I will be working on adding pagination to the CocktailsList page / styling of other components on our front-end using bootstrap and vanilla css. I plan on slicing the array into chunks of 20 and remapping over each chunk per page. This would later cause issues when trying to merge with eric's code which included the search component. I was able to add a 'sticky' navbar and included padding so that it would not overlap other components when initially loading.
***
After speaking with riley, my partner offered a solution to my logic. Using Lodash, I was able to write new logic that implemetned the search functionality and pagination with much less code. Tomorrow I wil continue to work on the front-end.

D15 7/24

D16 7/25
***
Yesterday I started working on trying to use tailwind to style some components such as the CocktailCard but ended up sticking with bootstrap and css.
***
Today I will be working on a BaseCocktailCard component to reuse across our application as well as making other style adjustments on the front-end. The rest of my group are working on other front-end components and fixing some issues with our ingredients search functionality.
***
I am running into some issues with the styling of different components  being overridden by other css styles at the moment. If I can't get the components styled correctly before stand-up tomorrow, I plan on asking my team members for help.


D17 7/26
***
Yesterday I continued work on some front-end components as we continue to wrap up our project, I was able to resolve my styling issues with the help of my teammate Lavanya.
***
Today I will be focusing on fixing some of the minor front-end issues. I was able to apply the grid format to the mapping of our cocktail cards on multiple pages by wrapping them in containers and using bootstrap. Mitch and I will be focusing on changing the color scheme to be uniform across our application, this includes buttons, navbar links, text-colors / styles.
***
While finishing our front-end I ran into an issue with some unwanted padding between our navbar and the first component on each page. I was unable to locate the source of the padding until Mitch was able to locate and remove it. We are more than 90% done with our project and will mainly be focused on fixing small details and making the application look nice.

D18 7/27
***
Yesterday Mitch and I focused on front-end components and overall styling of our application.
***
Today Mitch will be merging his rating feature into the main_page branch so that we can test everything before making our final merge to main! My goal today is to help put any finishing touches on new components that my teammates have been working on as we are treating the main_page branch as a testing ground for the styling of newly implemented features without messing up the main branch.
***
Currently we have no blockers in relation to our css, there are still some minor bugs we need to fix with new features but for the most part we have completed our project.

D19 7/28
***
Yesterday I focused on the finishing touches for our cocktail cards and their format on each page.
***
We are pretty much done with our project at this point, Eric is currently working on a comments feature that we hope to implement over stretch week while the rest of the group is making sure we check all the boxes on the rubric. I have nothing major targeted for today, just small style changes and edits.
***
No blockers at the moment!
