# Module3 Project Gamma

## Mix Masters

- Eric Chi
- Lavanya Nese
- Erick Emelio
- Mitch Cain

Mix Masters is an application that brings you various types of cocktails and mocktails to help you be the best bartender at a party!

## Design

- [GHI](docs/ghi.md)
- [Integrations](docs/integrations.md)
- [API](docs/api.md)
- [Data Models](docs/data-models.md)

## Intended Market

- We are targeting ambitious users who want to learn about various cocktails/mocktails and are interested in creating these drinks at home or at parties.

## Functionality

- Main Page features four random cocktails for users to start their cocktail/mocktail journey
- Main Page allows you to login or sign up
- Login page allows the user to login with credentials, if it is wrong an error message will inform the user
- If the user does not have credentials the login page also allows the user to sign up
- The sign up page allows the user to create a valid user and it automatically logs the user into the user page
- If user logs in they are greeted by their username and given a random cocktail on their User Page
- The logo button is clickable and it leads you back to the user page
- If user logs in they can save drinks into their favorites page
- Favorites page displays saved drink recipes
- About page shows the software engineers behind the application and their favorite cocktail with avatar, along with company details
- Cocktails list page shows all cocktails available in the database for users to browse and click to view their specific details, as well as search cocktails by name
- Search by ingredient page involves filtering cocktails by ingredients
- Search by alcohol page involves filtering cocktails by 6 common spirits: Brandy, Gin, Rum, Tequila, Whiskey, Vodka
- The search by alcohol allows the user to choose a cocktail from a drop down menu and submit it with a submit button, this will lead you to another page that will show a list of cocktails containing the chosen alcohol
- Cocktail detail pages show instructions on how to make the cocktail, ingredients and associated measurements, cocktail category, and associated photo and allows the user to put a star rating and favorite or unfavorite a drink
- Utilizes third party cocktail API

## Project Initialization

To run this application on your local machine please follow these steps:

1. Clone the repository to your local machine
2. CD (change directory) into the new project directory
3. Run `docker volume create jwtdown-db-data`
4. Run `docker compose build`
5. Run `docker compose up`
   ....

