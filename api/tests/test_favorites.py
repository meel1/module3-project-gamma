from fastapi.testclient import TestClient
from main import app
from queries.favorites import FavoriteQueries
from models import FavoriteIn
from authenticator import authenticator

client = TestClient(app)


def fake_get_current_account_data():
    return {"id": "1111", "username": "fake-user"}


class FakeFavoriteQueries:
    def create(self, favorite_in: FavoriteIn, account_id: str):
        favorite = favorite_in.dict()
        favorite["id"] = "fake-id-from-db"
        favorite["account_id"] = account_id
        return favorite

    def list_all_for_account(self, account_id: str):
        return [{"id": "11007", "account_id": "64aee2f53076373306cb67af"}]

    def delete(self, id: str, account_id: str):
        return {"success": True}


def test_create_favorite():
    app.dependency_overrides[FavoriteQueries] = FakeFavoriteQueries
    app.dependency_overrides[
        authenticator.get_current_account_data
    ] = fake_get_current_account_data
    favorite_in = {"id": "11007"}
    res = client.post("/api/favorites", json=favorite_in)
    data = res.json()
    assert res.status_code == 200
    assert data == {"id": "fake-id-from-db", "account_id": "1111"}


def test_list_favorites():
    app.dependency_overrides[FavoriteQueries] = FakeFavoriteQueries
    app.dependency_overrides[
        authenticator.get_current_account_data
    ] = fake_get_current_account_data
    res = client.get("/api/favorites/mine")
    data = res.json()
    assert res.status_code == 200
    assert data == {
        "favorites": [
            {"id": "11007", "account_id": "64aee2f53076373306cb67af"}
        ]
    }


def test_delete_favorite():
    app.dependency_overrides[FavoriteQueries] = FakeFavoriteQueries
    app.dependency_overrides[
        authenticator.get_current_account_data
    ] = fake_get_current_account_data
    res = client.delete("/api/favorites/11007")
    data = res.json()
    assert res.status_code == 200
    assert {"success": True} == data
