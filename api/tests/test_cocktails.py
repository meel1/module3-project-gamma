from fastapi.testclient import TestClient
from main import app
from queries.cocktails import CocktailQueries
from authenticator import authenticator

client = TestClient(app)
base_url = "https://www.thecocktaildb.com/images/media/drink/"


def fake_get_current_account_data():
    return {"id": "1111", "username": "fake-user"}


class TestCocktailQueries:
    def list_all_cocktails(self):
        return [
            {
                "name": "Mojito",
                "url": base_url + "metwgh1606770327.jpg",
                "id": "11000",
            },
        ]

    def get_cocktail_detail_by_name(self, name: str):
        return {
            "name": name,
            "category": "Cocktail",
            "instructions": """Muddle mint leaves with sugar and lime juice.
            Add a splash of soda water and fill the glass with cracked ice.
            Pour the rum and top with soda water.
            Garnish and serve with straw.""",
            "url": base_url + "metwgh1606770327.jpg",
            "ingredients_measurement": """Light rum: 2-3 oz ,
            Lime: Juice of 1 , Sugar: 2 tsp ,
            Mint: 2-4 , Soda water""",
            "id": "11000",
        }

    def list_cocktails_by_ingredient(self, ingredient: str):
        return [
            {
                "name": ingredient,
                "url": base_url + "tpxurs1454513016.jpg",
                "id": "16333",
            },
        ]

    def get_random_cocktail(self):
        return {
            "name": "Banana Strawberry Shake",
            "category": "Shake",
            "instructions": "Blend all together in a blender until smooth.",
            "url": base_url + "vqquwx1472720634.jpg",
            "ingredients_measurement": """Strawberries: 1/2 lb frozen ,
            Banana: 1 frozen , Yoghurt: 1 cup plain ,
            Milk: 1 cup , Honey:  to taste\n""",
            "id": "12656",
        }

    def list_ten_random_cocktails(self):
        return [
            {
                "name": "Tequila Slammer",
                "url": base_url + "43uhr51551451311.jpg",
                "id": "178307",
            },
        ]

    def get_one_cocktail(self, id: str):
        return {
            "name": "Mojito",
            "category": "Cocktail",
            "instructions": """Muddle mint leaves with sugar and lime juice.
            Add a splash of soda water and fill the glass with cracked ice.
            Pour the rum and top with soda water.
            Garnish and serve with straw.""",
            "url": base_url + "metwgh1606770327.jpg",
            "ingredients_measurement": """Light rum: 2-3 oz ,
            Lime: Juice of 1 , Sugar: 2 tsp ,
            Mint: 2-4 , Soda water""",
            "id": id,
        }

    def list_ingredients(self):
        return [
            {"ingredient": "Vodka"},
        ]


def test_list_all_cocktails():
    app.dependency_overrides[CocktailQueries] = TestCocktailQueries
    app.dependency_overrides[
        authenticator.get_current_account_data
    ] = fake_get_current_account_data
    res = client.get("/api/cocktails")
    data = res.json()
    assert res.status_code == 200
    assert data == {
        "cocktails": [
            {
                "name": "Mojito",
                "url": base_url + "metwgh1606770327.jpg",
                "id": "11000",
            },
        ]
    }


def test_get_cocktails_by_name():
    app.dependency_overrides[CocktailQueries] = TestCocktailQueries
    app.dependency_overrides[
        authenticator.get_current_account_data
    ] = fake_get_current_account_data
    res = client.get("/api/cocktails/name/test-cocktail")
    data = res.json()
    assert res.status_code == 200
    assert data == {
        "name": "test-cocktail",
        "category": "Cocktail",
        "instructions": """Muddle mint leaves with sugar and lime juice.
            Add a splash of soda water and fill the glass with cracked ice.
            Pour the rum and top with soda water.
            Garnish and serve with straw.""",
        "url": base_url + "metwgh1606770327.jpg",
        "ingredients_measurement": """Light rum: 2-3 oz ,
            Lime: Juice of 1 , Sugar: 2 tsp ,
            Mint: 2-4 , Soda water""",
        "id": "11000",
    }


def test_get_cocktails_by_ingredient():
    app.dependency_overrides[CocktailQueries] = TestCocktailQueries
    app.dependency_overrides[
        authenticator.get_current_account_data
    ] = fake_get_current_account_data
    res = client.get("/api/cocktails/ingredient/test-ingredient")
    data = res.json()
    assert res.status_code == 200
    assert data == {
        "cocktails": [
            {
                "name": "test-ingredient",
                "url": base_url + "tpxurs1454513016.jpg",
                "id": "16333",
            },
        ]
    }


def test_get_cocktail_by_id():
    app.dependency_overrides[CocktailQueries] = TestCocktailQueries
    res = client.get("/api/cocktails/test-id")
    data = res.json()
    assert res.status_code == 200
    assert data == {
        "name": "Mojito",
        "category": "Cocktail",
        "instructions": """Muddle mint leaves with sugar and lime juice.
            Add a splash of soda water and fill the glass with cracked ice.
            Pour the rum and top with soda water.
            Garnish and serve with straw.""",
        "url": base_url + "metwgh1606770327.jpg",
        "ingredients_measurement": """Light rum: 2-3 oz ,
            Lime: Juice of 1 , Sugar: 2 tsp ,
            Mint: 2-4 , Soda water""",
        "id": "test-id",
    }


def test_get_random_cocktail():
    app.dependency_overrides[CocktailQueries] = TestCocktailQueries
    res = client.get("/api/random/cocktail")
    data = res.json()
    assert res.status_code == 200
    assert data == {
        "name": "Banana Strawberry Shake",
        "category": "Shake",
        "instructions": "Blend all together in a blender until smooth.",
        "url": base_url + "vqquwx1472720634.jpg",
        "ingredients_measurement": """Strawberries: 1/2 lb frozen ,
            Banana: 1 frozen , Yoghurt: 1 cup plain ,
            Milk: 1 cup , Honey:  to taste\n""",
        "id": "12656",
    }


def test_get_ten_random_cocktails():
    app.dependency_overrides[CocktailQueries] = TestCocktailQueries
    res = client.get("/api/random/cocktails")
    data = res.json()
    assert res.status_code == 200
    assert data == [
        {
            "name": "Tequila Slammer",
            "url": base_url + "43uhr51551451311.jpg",
            "id": "178307",
        },
    ]


def test_list_all_ingredients():
    app.dependency_overrides[CocktailQueries] = TestCocktailQueries
    res = client.get("/api/ingredients")
    data = res.json()
    assert res.status_code == 200
    assert data == [
        {"ingredient": "Vodka"},
    ]
