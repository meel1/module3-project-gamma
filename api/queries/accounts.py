from models import AccountIn, AccountOutWithHashedPassword
from queries.client import MongoQueries


class DuplicateAccountError(ValueError):
    message: str


class AccountQueries(MongoQueries):
    collection_name = "accounts"

    def get(self, username: str):
        props = self.collection.find_one({"username": username})
        if props is None:
            return None
        props["id"] = str(props["_id"])
        return AccountOutWithHashedPassword(**props)

    def create(self, account_in: AccountIn, hashed_password: str):
        props = account_in.dict()
        if self.collection.find_one({"username": props["username"]}):
            raise DuplicateAccountError()
        props["hashed_password"] = hashed_password
        del props["password"]
        self.collection.insert_one(props)
        props["id"] = str(props["_id"])
        return AccountOutWithHashedPassword(**props)
