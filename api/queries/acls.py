from fastapi import HTTPException


def cocktail_details_template(result):
    if result is not None:
        result[0]["name"] = result[0].pop("strDrink")
        result[0]["category"] = result[0].pop("strCategory")
        result[0]["instructions"] = result[0].pop("strInstructions")
        result[0]["url"] = result[0].pop("strDrinkThumb")
        result[0]["id"] = result[0].pop("idDrink")
        ingredients_measurement = []
        for i in range(1, 16):
            ingredient = result[0].pop(f"strIngredient{i}")
            measure = result[0].pop(f"strMeasure{i}")
            if ingredient:
                if measure:
                    ingredients_measurement.append(
                        ingredient + ": " + measure
                    )
                else:
                    ingredients_measurement.append(ingredient)
        full_ingredients_measurement = []
        for item in ingredients_measurement:
            if item is not None:
                full_ingredients_measurement.append(item)
        result[0]["ingredients_measurement"] = ", ".join(
            full_ingredients_measurement
        )
        return result[0]
    else:
        raise HTTPException(status_code=404, detail="Cocktail not found")


def cocktail_list_template(result):
    content = []
    if result:
        for i in range(len(result)):
            result[i]["name"] = result[i].pop("strDrink")
            result[i]["url"] = result[i].pop("strDrinkThumb")
            result[i]["id"] = result[i].pop("idDrink")
            content.append(result[i])
        return content
    else:
        raise HTTPException(status_code=404, detail="No cocktails found")


def ingredients_list_template(result):
    content = []
    if result:
        for i in range(len(result)):
            result[i]["ingredient"] = result[i].pop("strIngredient1")
            content.append(result[i])
        return content
