from models import FavoriteIn
from queries.client import MongoQueries
from fastapi import HTTPException


class FavoriteQueries(MongoQueries):
    collection_name = "favorites"

    def create(
        self,
        favorite_in: FavoriteIn,
        account_id: str,
    ):
        favorite = favorite_in.dict()
        favorite["account_id"] = account_id
        search = self.find_one(account_id, favorite_in.id)
        if search:
            raise HTTPException(
                status_code=404,
                detail="This cocktail ID already in favorites!",
            )
        result = self.collection.insert_one(favorite)
        if result.inserted_id:
            favorite["id"] = str(result.inserted_id)
            return favorite

    def find_one(self, account_id: str, id: str):
        result = self.collection.find_one({"account_id": account_id, "id": id})
        if result is not None:
            result["id"] = str(result["id"])
        return result

    def list_all_for_account(self, account_id: str):
        results = []
        for favorite in self.collection.find({"account_id": account_id}):
            favorite["id"] = str(favorite["id"])
            results.append(favorite)
        return results

    def delete(self, id: str, account_id: str):
        result = self.collection.delete_one(
            {"id": id, "account_id": account_id}
        )
        return result.deleted_count > 0
