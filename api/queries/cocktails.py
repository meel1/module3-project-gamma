from queries.client import MongoQueries
from .acls import (
    cocktail_details_template,
    cocktail_list_template,
    ingredients_list_template,
)

import requests
import os
import operator

COCKTAIL_API_KEY = os.environ["COCKTAIL_API_KEY"]
base_url = f"https://www.thecocktaildb.com/api/json/v2/{COCKTAIL_API_KEY}/"


class CocktailQueries(MongoQueries):
    collections_name = "cocktails"

    def list_all_cocktails(self):
        url_additions = "filter.php?a="
        url1 = base_url + url_additions + "Alcoholic"
        url2 = base_url + url_additions + "Non_Alcoholic"
        url3 = base_url + url_additions + "Optional_Alcohol"
        res1 = requests.get(url1)
        res2 = requests.get(url2)
        res3 = requests.get(url3)
        data1 = res1.json()
        data2 = res2.json()
        data3 = res3.json()
        result = (
            data1.get("drinks") + data2.get("drinks") + data3.get("drinks")
        )
        sorted_content = sorted(
            cocktail_list_template(result), key=operator.itemgetter("id")
        )
        return sorted_content

    def get_cocktail_detail_by_name(self, name: str):
        url_additions = f"search.php?s={name}"
        res = requests.get(base_url + url_additions)
        data = res.json()
        result = data["drinks"]
        return cocktail_details_template(result)

    def list_cocktails_by_ingredient(self, ingredient: str):
        url_additions = f"filter.php?i={ingredient}"
        res = requests.get(base_url + url_additions)
        data = res.json()
        result = data["drinks"]
        return cocktail_list_template(result)

    def get_one_cocktail(self, id: str):
        url_additions = f"lookup.php?i={id}"
        res = requests.get(base_url + url_additions)
        data = res.json()
        result = data["drinks"]
        return cocktail_details_template(result)

    def get_random_cocktail(self):
        url_additions = "random.php"
        res = requests.get(base_url + url_additions)
        data = res.json()
        result = data["drinks"]
        return cocktail_details_template(result)

    def list_ten_random_cocktails(self):
        url_additions = "randomselection.php"
        res = requests.get(base_url + url_additions)
        data = res.json()
        result = data["drinks"]
        return cocktail_list_template(result)

    def list_ingredients(self):
        base_url_v1 = "https://www.thecocktaildb.com/api/json/v1/1/"
        url_additions = "list.php?i=list"
        res = requests.get(base_url_v1 + url_additions)
        data = res.json()
        result = data["drinks"]
        sorted_content = sorted(
            ingredients_list_template(result),
            key=operator.itemgetter("ingredient"),
        )
        return sorted_content
