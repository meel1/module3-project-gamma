from bson.objectid import ObjectId
from pydantic import BaseModel
from typing import List, Optional
from bson import errors


class PydanticObjectId(ObjectId):
    @classmethod
    def __get_validators__(cls):
        yield cls.validate

    @classmethod
    def validate(cls, value: ObjectId | str) -> ObjectId:
        if value:
            try:
                ObjectId(value)
            except errors.InvalidId:
                raise ValueError(f"Not a valid object id: {value}")
        return value


class FavoriteIn(BaseModel):
    id: str


class FavoriteOut(BaseModel):
    id: str
    account_id: str


class Favorites(BaseModel):
    favorites: List[FavoriteOut]


class Account(BaseModel):
    id: PydanticObjectId


class AccountIn(BaseModel):
    username: str
    password: str
    full_name: str
    email: str


class AccountOut(BaseModel):
    id: str
    username: str
    full_name: str
    email: str


class AccountOutWithHashedPassword(AccountOut):
    hashed_password: str


class CocktailOut(BaseModel):
    name: str
    url: str
    id: str


class CocktailList(BaseModel):
    cocktails: List[CocktailOut]


class CocktailListWithIngredient(BaseModel):
    cocktails: List[CocktailOut]
    ingredient: str


class CocktailIngredient(BaseModel):
    name: str
    measurement: str


class CocktailDetails(BaseModel):
    name: str
    category: str
    instructions: Optional[str]
    url: Optional[str]
    ingredients_measurement: Optional[str]
    id: str


class Ingredients(BaseModel):
    ingredient: str
