from fastapi import APIRouter, Depends
from models import (
    CocktailOut,
    CocktailDetails,
    CocktailList,
    Ingredients
    )
from queries.cocktails import CocktailQueries
from typing import List
from authenticator import authenticator


router = APIRouter()


@router.get("/api/cocktails", response_model=CocktailList)
def list_all_cocktails(
    account_data: dict = Depends(authenticator.get_current_account_data),
    queries: CocktailQueries = Depends(),
):
    return {"cocktails": queries.list_all_cocktails()}


@router.get("/api/cocktails/name/{name}", response_model=CocktailDetails)
def get_cocktails_by_name(
    name: str,
    account_data: dict = Depends(authenticator.get_current_account_data),
    queries: CocktailQueries = Depends(),
):
    return queries.get_cocktail_detail_by_name(name)


@router.get(
    "/api/cocktails/ingredient/{ingredient}", response_model=CocktailList
)
def get_cocktails_by_ingredient(
    ingredient: str,
    account_data: dict = Depends(authenticator.get_current_account_data),
    queries: CocktailQueries = Depends(),
):
    return {
        "cocktails": queries.list_cocktails_by_ingredient(ingredient),
        "ingredient": ingredient,
    }


@router.get("/api/cocktails/{id}", response_model=CocktailDetails)
def get_cocktail_by_id(
    id: str,
    account_data: dict = Depends(authenticator.get_current_account_data),
    queries: CocktailQueries = Depends(),
):
    return queries.get_one_cocktail(id)


@router.get("/api/random/cocktail", response_model=CocktailDetails)
def get_random_cocktail(
    account_data: dict = Depends(authenticator.get_current_account_data),
    queries: CocktailQueries = Depends(),
):
    return queries.get_random_cocktail()


@router.get("/api/random/cocktails", response_model=List[CocktailOut])
def get_ten_random_cocktails(queries: CocktailQueries = Depends()):
    return queries.list_ten_random_cocktails()


@router.get("/api/ingredients/", response_model=List[Ingredients])
def list_all_ingredients(queries: CocktailQueries = Depends()):
    return queries.list_ingredients()
