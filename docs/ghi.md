# User Graphical Human Interface

## Full Wireframe
![Wireframe Full](docs/image-28.png)
## Main Page
![Main Page](docs/image-20.png)
## User Page
![User Page](docs/image-17.png)
## About Page
![About](docs/image-18.png)
## Cocktails List Page
![Cocktails List](docs/image-27.png)
## Cocktail Detail Page
![Cocktail Detail](docs/image-13.png)
## Search by Ingredients Page
![Search by Ingredient](docs/image-24.png)
## Search by Name Page
![Search By Name](docs/image-26.png)
## Search by Alcohol Page
![Search by Alcohol](docs/image-14.png)
## Result page for Search by Alcohol Page
![Search by Alcohol](docs/image-23.png)

## Favorites Page
![Favorites](docs/image-25.png)
## Login Page
![Login Page](docs/image-19.png)
## Sign Up Page
![Sign Up](docs/image-16.png)
