# Data models

## Cocktails
### CocktailOut

| name             | type   |
| ---------------- | ------ |
| name             | string |
| url              | string |
| id               | string |

### CocktailList
List[CocktailOut]

### CocktailDetails

| name                    | type   | unique | optional |
| ----------------------- | ------ | ------ | -------- |
| name                    | string | yes    | no       |
| category                | string | no     | no       |
| instructions            | string | no     | yes      |
| url                     | string | no     | yes       |
| ingredients_measurement | string | no     | yes      |
| id                      | int    | yes    | no       |

### CocktailIngredient

| name         | type   |
| -------------| ------ |
| name         | string |
| measurement  | string |

### Ingredients

| name         | type   |
| -------------| ------ |
| ingredient   | string |

## Favorites
### FavoriteIn
| name             | type   |
| ---------------- | ------ |
| id               | string |

### FavoriteOut
| name             | type   |
| ---------------- | ------ |
| name             | string |
| account_id       | string |

### Favorites
List[FavoriteOut]

## Account
### AccountIn
 name          | type   |
| ------------ | ------ |
| username     | string |
| password     | string |
| full_name    | string |
| email        | string |

### AccountOut
 name          | type   |
| ------------ | ------ |
| username     | string |
| id           | string |
| full_name    | string |
| email        | string |

### AccountOutWithHashedPassword
name                  | type   |
| ------------------- | ------ |
| hashed_password     | string |
