# APIs
## Cocktails

### List All Cocktails:
#### Method: `GET`
#### Endpoint URL: `/api/cocktails`
#### Output:
```json
{
  "cocktails": [
    {
      "name": "string",
      "url": "string",
      "id": "string"
    }
  ]
}
```

### Get Cocktails by Name:
#### Method: `GET`
#### Endpoint URL: `/api/cocktails/name/{name}`
#### Input:
```json
{
  "name": "string"
}
```
#### Output:
```json
{
  "name": "string",
  "category": "string",
  "instructions": "string",
  "url": "string",
  "ingredients_measurement": "string",
  "id": "string"
}
```

### Get Cocktails by Ingredient:
#### Method: `GET`
#### Endpoint URL: `/api/cocktails/ingredients/{ingredients}`
#### Input:
```json
{
  "ingredient": "string"
}
```
#### Output:
```json
[
  {
    "name": "string",
    "url": "string",
    "id": "string"
  }
]
```

## Get Cocktails by ID:
#### Method: `GET`
#### Endpoint URL: `/api/cocktails/{id}`
#### Input:
```json
{
  "id": "string"
}
```
#### Output:
```json
{
  "name": "string",
  "category": "string",
  "instructions": "string",
  "url": "string",
  "ingredients_measurement": "string",
  "id": "string"
}
```
## Get Random Cocktail:
#### Method: `GET`
#### Endpoint URL: `/api/random/cocktail`
#### Output:
```json
{
  "name": "string",
  "category": "string",
  "instructions": "string",
  "url": "string",
  "ingredients_measurement": "string",
  "id": "string"
}
```
## Get Ten Random Cocktails:
#### Method: `GET`
#### Endpoint URL: `/api/random/cocktails`
#### Output:
```json
[
  {
    "name": "string",
    "url": "string",
    "id": "string"
  }
]
```
## List All Ingredients:
#### Method: `GET`
#### Endpoint URL: `/api/ingredients`
#### Output:
```json
[
  {
    "ingredient": "string"
  }
]
```

## Favorites

### Get all favorites

#### Method: `GET`
#### Endpoint URL: `/api/favorites/mine`
#### Output:

```json
{
  "favorites": [
    {
      "id": "string",
      "account_id": "string"
    }
  ]
}
```

### Create favorites
#### Method: `POST`
#### Endpoint URL: `/api/favorites
#### Input:
```json
{
  "id": "string"
}
```
#### Output:

```json
{
  "id": "string",
  "account_id": "string"
}
```

### Delete favorites
#### Method: `DELETE`
#### Endpoint URL: `/api/favorites/{id}`
#### Input:
```json
{
  "id": "string"
}
```
#### Output:

```json
{
  true
}
```

## Accounts

### Login
#### Method: `POST`
#### Endpoint URL: `/token`
#### Input:

```json
{
  "username": string,
  "password": string,
}
```

#### Output:
```json
{
  "access_token": "string",
  "token_type": "Bearer"
}
```

### Logout
#### Method: `DELETE`
#### Endpoint URL: `/token`

#### Output:
```json
{
  true
}
```

### Get Token
#### Method: `GET`
#### Endpoint URL: `/token`
#### Output:
```json
{
  "access_token": "string",
  "token_type": "Bearer",
  "account": {
    "id": "string",
    "username": "string",
    "full_name": "string",
    "email": "string"
  }
}
```

### Get Protected
#### Method: `GET`
#### Endpoint URL: `/protected`
#### Output:
```json
{
  true
}
```

### Create Account
#### Method: `POST`
#### Endpoint URL: `/api/accounts`

#### Input:

```json
{
  "username": "string",
  "password": "string",
  "full_name": "string",
  "email": "string"
}
```

#### Output:

```json
{
  "access_token": "string",
  "token_type": "Bearer",
  "account": {
    "id": "string",
    "username": "string",
    "full_name": "string",
    "email": "string"
  }
}
```
